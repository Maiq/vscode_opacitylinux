#!/bin/bash 

# OUTPUT=`code && sleep 2 && xwininfo -root -tree | (grep code) | (awk '{print $1;}') | (head -n 1) `

if [ "$#" -gt 0 ]
then
    if [ "$1" -lt 10 ]
    then
        OPACITY=10
    elif [ "$1" -gt 100 ]
    then OPACITY=100
    else OPACITY=$1
    fi
else
    OPACITY=75
fi

OUTPUT=`xwininfo -root -tree | (grep code) | (awk '{print $1;}') | (head -n 1) `
echo "${OUTPUT}"
`xprop -id ${OUTPUT} -f _NET_WM_WINDOW_OPACITY 32c -set _NET_WM_WINDOW_OPACITY $(printf 0x%x $((0xffffffff * $OPACITY / 100)))`


